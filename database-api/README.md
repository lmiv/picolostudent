#   Deployment of back-end
### MySQL Docker Container
The MySQL server needs to be setup within a docker container. A [dockerfile](/databse-api/Dockerfile) to create an image and a [docker-compose](/docker-compose.yml) file to run the container.
Open a terminal in this folder, and first run the following command:
```shell
docker build -t custom_mysql_img:1.0 .
```
Now the image has been created on your system, check this by running the following command:
```shell
docker images
```
If all gone well an image with the name `custom_mysql_img` should be in the list.

Next up is composing our container, use the following command to do so:
```shell
docker-compose up
```
Your MySQL Docker Container is now running.

### Spring Boot API Docker Container
