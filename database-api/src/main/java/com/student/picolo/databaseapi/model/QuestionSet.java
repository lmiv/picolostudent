package com.student.picolo.databaseapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Entity
@Table(name = "question_sets")
public class QuestionSet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "questionset_id")
    private List<Question> questions = new LinkedList<>();

    public QuestionSet(String title) {
        this.title = title;
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public void removeQuestion(Question question) {
        questions.remove(question);
    }

    /**
     * Prepares the list of questions to contain specific names
     *
     * @param names random list of player names
     * @return list of questions with filled in names
     */
    public List<Question> preparedQuestions(List<String> names) {

        return questions.stream()
                .map(q -> q.fillInNames(names))
                .collect(Collectors.toList());
    }
}
