package com.student.picolo.databaseapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.student.picolo.databaseapi.util.PlaceHolders;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Slf4j
@NoArgsConstructor
@Entity
@Table(name = "questions")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "question")
    private String question;

    @Column(name = "amount_of_names")
    private int amountOfNames = 0;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "questionset_id")
    private QuestionSet questionSet;

    public Question(int id, String title, String question, int amountOfNames, QuestionSet questionSet) {
        this.id = id;
        this.title = title;
        this.question = question;
        this.questionSet = questionSet;
        this.amountOfNames = amountOfNames;
    }

    /**
     * This funtion fills the question in with random names and returns an instance of Question with the names filled in
     *
     * @param names: List of names of the players to pick from.
     *
     * @return Question instance with names filled out.
     */
    public Question fillInNames(List<String> names) {
        //  Shuffle the list of names
        Collections.shuffle(names);
        Iterator<String> iterator = names.listIterator();

        log.info("names : {}", names);

        // Split question into individual words
        String[] words = question.split(" ");
        String question = Arrays.stream(words)
                .map((w) -> {
                    if (w.contains(PlaceHolders.NAME)) {
                        return iterator.next();
                    } else {
                        return w;
                    }
                })
                .collect(Collectors.joining(" "));

        this.setQuestion(question);

        return this;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", question='" + question + '\'' +
                ", amountOfNames=" + amountOfNames +
                '}';
    }
}
