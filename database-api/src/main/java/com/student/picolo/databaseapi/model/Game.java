package com.student.picolo.databaseapi.model;

import lombok.Data;

import java.util.List;

public interface Game {
    QuestionSet getQuestionSet();

    List<String> getNames();

    int getQuestionIterator();

    void setQuestionIterator(int i);
}
