package com.student.picolo.databaseapi.controller;

import com.student.picolo.databaseapi.model.Game;
import com.student.picolo.databaseapi.model.Question;
import com.student.picolo.databaseapi.model.QuestionSet;
import com.student.picolo.databaseapi.repository.GameRepositoryImpl;
import com.student.picolo.databaseapi.repository.QuestionSetRepositoryImpl;
import com.student.picolo.databaseapi.util.PlaceHolders;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

//  TODO: Find out which kind of Param can best be used for the different params

@RestController
@RequestMapping("/api")
@Slf4j
public class GameController {

    @Autowired
    private GameRepositoryImpl gameRepository;

    @Autowired
    private QuestionSetRepositoryImpl questionSetRepository;

    /**
     * Create a new game
     *
     * @param questionSetTitle: title of questionSet linked to this game.
     * @param names          : list of player names
     * @return id : of created game
     */
    @GetMapping("/create-game/{questionSetTitle}")
    public int createGame(@PathVariable String questionSetTitle, @RequestParam List<String> names) {
        QuestionSet questionSet = questionSetRepository.getQuestionSet(questionSetTitle);
        return gameRepository.createNewGame(questionSet, names);
    }

    /**
     * Get next question from game
     *
     * @param gameId from the game you want to request the next question of.
     * @return the next Question if present. Otherwise empty question template.
     */
    @GetMapping("/get-next-question/{gameId}")
    public Question nextQuestion(@PathVariable int gameId) {
        try {
            //  TODO: Find neater solution for this
            //  JsonQuestion rather then Question instance, because otherwise we get a recurrent call to QuestionSet
            return gameRepository.getNextQuestion(gameRepository.getGame(gameId));
        } catch (IndexOutOfBoundsException e) {
            Game game = gameRepository.getGame(gameId);
            log.info("End of game, deleting : {}", game);
            gameRepository.deleteGame(game);
            return PlaceHolders.END_QUESTION;
        }
    }

    @GetMapping("/get-question-sets")
    public List<QuestionSet> listQuestionSet() {
        return questionSetRepository.listAll();
    }

    @GetMapping("/get-question-sets-by-id/{ids}")
    public List<QuestionSet> listQuestionSetByIds(@PathVariable List<Integer> ids) {
        return questionSetRepository.listAll().stream().filter(n -> ids.contains(n.getId() - 1)).collect(Collectors.toList());
    }

    @GetMapping("/create-question-set/{title}")
    public int createQuestionSet(@PathVariable String title) {
        return questionSetRepository.addQuestionSet(title);
    }

    @PostMapping("/create-question-set")
    public int createQuestionSet(@RequestBody QuestionSet questionSet) {
        return questionSetRepository.addQuestionSet(questionSet);
    }

    @PostMapping("/update-question-set")
    public void updateQuestionSet(@RequestBody QuestionSet questionSet) {
        questionSetRepository.updateQuestionSet(questionSet);
    }

    @PostMapping("/delete-question-set")
    public void deleteQuestionSet(@RequestBody QuestionSet questionSet) {
        questionSetRepository.deleteQuestionSet(questionSet);
    }

    @PostMapping("/add-question/{questionSetId}")
    public void addQuestion(@RequestBody Question question, @PathVariable int questionSetId) {
        questionSetRepository.addQuestion(question, questionSetId);
    }
}
