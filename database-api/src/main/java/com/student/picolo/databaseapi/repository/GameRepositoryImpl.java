package com.student.picolo.databaseapi.repository;

import com.student.picolo.databaseapi.model.Game;
import com.student.picolo.databaseapi.model.GameImpl;
import com.student.picolo.databaseapi.model.Question;
import com.student.picolo.databaseapi.model.QuestionSet;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
* An implementation to interact with the database with game
*/
@Repository
@Slf4j
public class GameRepositoryImpl implements GameRepository{

    private EntityManager entityManager;

    @Autowired
    public GameRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public int createNewGame(QuestionSet questionSet, List<String> names) {
        try (Session session = entityManager.unwrap(Session.class)){
            log.info("[GameRepository]\t creating a new game for QuestionSet : {}\n With players : {}", questionSet, names);
            Game game = new GameImpl(questionSet, names);
            return (int) session.save(game);
        }
    }

    @Override
    @Transactional
    public Game getGame(int id) {
        try (Session session = entityManager.unwrap(Session.class)){
            log.info("[GameRepository]\t Getting a game instance with id : {}", id);
            return session.get(GameImpl.class, id);
        }
    }

    @Override
    @Transactional
    public void deleteGame(Game toDelete) {
        try (Session session = entityManager.unwrap(Session.class)){
            log.info("[GameRepository]\t Deleting : {}", toDelete);
            session.delete(toDelete);
        }
    }

    @Override
    @Transactional
    public Question getNextQuestion(Game game) throws IndexOutOfBoundsException {

        try (Session session = entityManager.unwrap(Session.class)) {
            Question question =game.getQuestionSet().preparedQuestions(game.getNames()).get(game.getQuestionIterator());
            game.setQuestionIterator(
                    game.getQuestionIterator()+1
            );
            session.update(game);
            return question;
        } catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException("No more questions in the game : " + this );
        }
    }
}
