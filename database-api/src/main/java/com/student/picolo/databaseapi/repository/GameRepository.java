package com.student.picolo.databaseapi.repository;

import com.student.picolo.databaseapi.model.Game;
import com.student.picolo.databaseapi.model.GameImpl;
import com.student.picolo.databaseapi.model.Question;
import com.student.picolo.databaseapi.model.QuestionSet;

import java.util.List;

public interface GameRepository {

    /**
     * This method is a method to add a game to the database
     *
     * @param questionSet The questionSet which will be played in this game
     * @param names       A list of names of the players participating in this game
     * @return the id of this game
     */
    int createNewGame(QuestionSet questionSet, List<String> names);

    /**
     * This method gets a specific game from the database.
     *
     * @param id from requestion game
     * @return instance of requested game
     */
    Game getGame(int id);

    void deleteGame(Game game);

    /**
     * A method to get the next question of the game with the names filled in
     *   @throws IndexOutOfBoundsException when there are no more questions in the game
     *  @return instance of the next question in the game wil the names filled out
     */
    Question getNextQuestion(Game game) throws IndexOutOfBoundsException;
}
