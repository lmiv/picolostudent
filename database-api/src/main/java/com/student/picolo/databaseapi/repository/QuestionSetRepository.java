package com.student.picolo.databaseapi.repository;

import com.student.picolo.databaseapi.model.Question;
import com.student.picolo.databaseapi.model.QuestionSet;

import java.util.List;

public interface QuestionSetRepository {


    /**
     * This function gets an instance of a specific question with a specified id
     *
     * @param id of the question instance we want to retrieve.
     *
     * @return retrieved QuestionSet
     * */
    QuestionSet getQuestionSet(int id);

    QuestionSet getQuestionSet(String title);

    List<QuestionSet> listAll();

    int addQuestionSet(String title);

    int addQuestionSet(QuestionSet questionSet);

    void updateQuestionSet(QuestionSet toUpdate);

    void deleteQuestionSet(QuestionSet toDelete);

    void addQuestion(Question toAdd, int questionSetId);
}
