package com.student.picolo.databaseapi.util;

import com.student.picolo.databaseapi.model.Question;

public class PlaceHolders {

    public static final String NAME = "NAME";

    public static final Question END_QUESTION = new Question(-1, "The End!", "This is the end of the game.", 0 ,null);

    private PlaceHolders(){}
}
