package com.student.picolo.databaseapi.repository;

import com.student.picolo.databaseapi.model.Question;
import com.student.picolo.databaseapi.model.QuestionSet;

public interface QuestionRepository {

    /**
     * A method which created a question in the database
     *
     * @param toAdd: the Question instance to add.
     */
    void createQuestion(Question toAdd);
}
