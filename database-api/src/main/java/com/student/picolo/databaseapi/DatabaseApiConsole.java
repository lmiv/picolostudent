//package com.student.picolo.databaseapi;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//
//import java.util.Scanner;
//
//@Slf4j
//@SpringBootApplication
//public class DatabaseApiConsole implements CommandLineRunner {
//
//    private static final Scanner scanner =  new Scanner(System.in);
//
//    public static void main(String[] args) {
//        SpringApplication.run(DatabaseApiConsole.class, args);
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//
//        //  Print main menu
//        boolean quit = false;
//        while (!quit) {
//            switch (printMenu()) {
//                case 1:
//                    startGame();
//                    break;
//                case 2:
//                    addQuestionSet();
//                    break;
//                case 3:
//                    addQuestion();
//                    break;
//                case 4:
//                    System.out.println("Exiting...");
//                    quit = true;
//                    break;
//                default:
//                    System.out.println("INVALID CHOICE!!");
//                    break;
//            }
//        }
//
//
//    }
//
//    private int printMenu(){
//        System.out.println("============================================");
//        System.out.println("Welkom bij de console versie van picolo");
//        System.out.println("============================================");
//        System.out.println("1.\tStart nieuw spel!");
//        System.out.println("2.\tVoeg nieuwe vragen-set toe!");
//        System.out.println("3.\tVoeg vraag toe!");
//        System.out.println("4.\tExit");
//
//        int choice = scanner.nextInt();
//        scanner.nextLine();
//        return  choice;
//
//    }
//
//    private void startGame(){
//        int newGameId = 1;
//    }
//
//    private void addQuestionSet() {
//    }
//
//    private void addQuestion() {
//    }
//}
