package com.student.picolo.databaseapi.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Iterator;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "games")
public class GameImpl implements Game{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "questionset_id")
    private QuestionSet questionSet;

    @Column(name = "iterator")
    private int questionIterator;

    @ElementCollection
    @CollectionTable(name = "game_names", joinColumns = @JoinColumn(name = "game_id"))
    @Column(name = "name")
    private List<String> names;

    public GameImpl(QuestionSet questionSet, List<String> names) {
        this.questionSet = questionSet;
        this.names = names;
        this.questionIterator = 0;
    }

    @Override
    public String toString() {
        return "GameImpl{" +
                "id=" + id +
                ", questionSetId=" + questionSet.getId() +
                ", questionIterator=" + questionIterator +
                ", names=" + names +
                '}';
    }
}
