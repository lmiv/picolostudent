package com.student.picolo.databaseapi.repository;

import com.student.picolo.databaseapi.model.Question;
import com.student.picolo.databaseapi.model.QuestionSet;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation to interact with the database with questionsets
 */
@Repository
@Slf4j
public class QuestionSetRepositoryImpl implements QuestionSetRepository{

    private EntityManager entityManager;

    @Autowired
    public QuestionSetRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public QuestionSet getQuestionSet(String title) {
        try (Session session = entityManager.unwrap(Session.class)){
            log.info("[QuestionSetRepository]\tGetting QuestionSet with id : {}", title);
            Query query = session.createQuery("FROM QuestionSet WHERE title = :title");
            query.setParameter("title", title);
            List<QuestionSet> list = query.getResultList();
            return list.get(0);
        }
    }

    @Override
    @Transactional
    public QuestionSet getQuestionSet(int id) {
        try (Session session = entityManager.unwrap(Session.class)){
            log.info("[QuestionSetRepository]\tGetting QuestionSet with id:  {}", id);
            return session.get(QuestionSet.class, id);
        }
    }

    @Override
    @Transactional
    public int addQuestionSet(String title) {
        QuestionSet toAdd = new QuestionSet(title);
        try (Session session = entityManager.unwrap(Session.class)) {
            log.info("[QuestionSetRepository]\tAdding QuestionSet : {}", title);
            session.save(toAdd);
            log.info("[QuestionSetRepository]\tAdded QuestionSet : {}", title);
        }
        return toAdd.getId();
    }

    @Override
    @Transactional
    public int addQuestionSet(QuestionSet questionSet) {
        int id = -1;
        try (Session session = entityManager.unwrap(Session.class)) {
            log.info("[QuestionSetRepository]\tAdding QuestionSet : {}", questionSet);
            id = (int) session.save(questionSet);
            log.info("[QuestionSetRepository]\tAdded QuestionSet : {}", questionSet);
        }
        return id;
    }

    @Override
    @Transactional
    public void updateQuestionSet(QuestionSet toUpdate) {
        try (Session session = entityManager.unwrap(Session.class)){
            session.update(toUpdate);
            log.info("[QuestionSetRepository]\tUpdate QuestionSet : {}", toUpdate);
        }
    }

    @Override
    @Transactional
    public void deleteQuestionSet(QuestionSet toDelete) {
        try (Session session = entityManager.unwrap(Session.class)){
            session.delete(toDelete);
            log.info("[QuestionSetRepository]\tDeleted QuestionSet : {}", toDelete);
        }
    }

    @Override
    @Transactional
    public List<QuestionSet> listAll() {
        try (Session session = entityManager.unwrap(Session.class)){

            Query query = session.createQuery("FROM QuestionSet ");
            List<QuestionSet> sets = query.getResultList();

            if (sets != null) {
                return sets;
            }

            return new ArrayList<>();
        }
    }

    @Override
    @Transactional
    public void addQuestion(Question toAdd, int questionSetId) {
        try (Session session = entityManager.unwrap(Session.class)) {
            QuestionSet qs = session.get(QuestionSet.class, questionSetId);
            qs.addQuestion(toAdd);
            log.info("updating questionset : {}", qs);
            session.update(qs);
        }
    }
}
