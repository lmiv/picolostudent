package com.student.picolo.databaseapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;

@Data
public class JsonQuestion {

    private int id;

    private String title;

    private String question;

    public JsonQuestion(Question question){
        this.id = question.getId();
        this.title = question.getTitle();
        this.question = question.getQuestion();
    }

}
