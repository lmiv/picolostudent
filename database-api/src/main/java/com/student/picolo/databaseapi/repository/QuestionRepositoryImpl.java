package com.student.picolo.databaseapi.repository;

import com.student.picolo.databaseapi.model.Question;
import com.student.picolo.databaseapi.model.QuestionSet;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

/**
 * An implementation to interact with the database with questions
 */
@Repository
@Slf4j
public class QuestionRepositoryImpl implements QuestionRepository{

    private EntityManager entityManager;

    @Autowired
    public QuestionRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void createQuestion(Question toAdd) {
        try (Session session = entityManager.unwrap(Session.class)){
            session.save(toAdd);
            log.info("[QuestionRepository]\tAdded Question : {}", toAdd);
        }
    }


}
