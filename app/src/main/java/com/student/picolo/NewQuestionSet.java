package com.student.picolo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.student.picolo.controller.ApiController;
import com.student.picolo.model.Question;

import java.util.LinkedList;
import java.util.List;

public class NewQuestionSet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_questionset);
        List<Question> questions = new LinkedList<>();
        EditText title = (EditText) findViewById(R.id.textTitle);
        EditText question = (EditText) findViewById(R.id.questionText);
        Button add = (Button) findViewById(R.id.button_add_question);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String t = title.getText().toString();
                String q = question.getText().toString();
                title.setText("");
                question.setText("");
                int count = (q.length() - q.replaceAll("NAME", "").length()) / 4;
                questions.add(new Question(t, q, count));
            }
        });

        Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questions.forEach(q -> ApiController.getInstance().addQuestion(q));
                Toast.makeText(NewQuestionSet.this, "Saving", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(), HomeScreen.class);
                startActivityForResult(intent, 0);
            }
        });

    }
}
