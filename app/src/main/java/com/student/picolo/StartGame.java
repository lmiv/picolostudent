package com.student.picolo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import com.student.picolo.controller.ApiController;
import com.student.picolo.model.QuestionSet;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class StartGame extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private final ArrayList<String> names = new ArrayList<>();
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (AUTO_HIDE) {
                        delayedHide(AUTO_HIDE_DELAY_MILLIS);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    view.performClick();
                    break;
                default:
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.game_start);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.buttonPlay).setOnTouchListener(mDelayHideTouchListener);
        Button playGame;
        final EditText name;
        Button enterName;
        Spinner questionSets;

        //todo Fill array sets with data from database instead of standard values


//        String[] sets = qsets.stream().map(QuestionSet::getTitle).toArray(String[]::new);

        final TextView nameBox;

        name = (EditText) findViewById(R.id.textName);
        playGame = (Button) findViewById(R.id.buttonPlay);
        enterName = (Button) findViewById(R.id.enterNameButton);
        questionSets = (Spinner) findViewById(R.id.questionSets);
        nameBox = (TextView) findViewById(R.id.namesBox);

        enterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                names.add(name.getText().toString());
                name.setText("");
                String list = "";
                if (!names.isEmpty()) {
                    int i = 0;
                    list = names.get(i);
                    i++;
                    while (i < names.size()) {
                        list += ", " + names.get(i);
                        i++;
                    }
                }

                nameBox.setText(list);
            }
        });

        name.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    if (keyCode == KeyEvent.KEYCODE_ENTER){
                        names.add(name.getText().toString());
                        name.setText("");
                        String list = "";
                        if (!names.isEmpty()) {
                            int i = 0;
                            list = names.get(i);
                            i++;
                            while (i < names.size()) {
                                list += ", " + names.get(i);
                                i++;
                            }
                        }

                        nameBox.setText(list);
                        return true;
                    }
                }
                return false;
            }
        });

        List<String> qsets = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, qsets);

        RetrofitInterface retrofitInterface = RetrofitInstance.getRetrofitInstance().create(RetrofitInterface.class);

        List<Integer> ids = new ArrayList<>();

        try {
            FileInputStream fileInputStream = openFileInput("favorite_sets.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                Log.i("line ids", lines);
                ids.add(Integer.parseInt(lines));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //  This part is inplace such that Retrofit sends the proper formatting for a list of strings, such that the API can interpret it correct.
        Iterable names_itr = Arrays.asList(ids);
        String str = names_itr.toString().replaceAll("\\s+", "").replaceAll("\\[+", "").replaceAll("\\]+", "");

        Call<List<QuestionSet>> listCall = retrofitInterface.getAllQuestionSetsFromId(str);

        listCall.enqueue(new Callback<List<QuestionSet>>() {
            @Override
            public void onResponse(Call<List<QuestionSet>> call, Response<List<QuestionSet>> response) {

                if (response.isSuccessful()) {
                    String rep = response.toString();
                    Log.i("Response", rep);
                    Log.i("Content", response.body().toString());

                    for (QuestionSet set : response.body()) {
                        qsets.add(set.getTitle());
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        questionSets.setAdapter(adapter);
                    }
                } else {
                    qsets.add("Code : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<QuestionSet>> call, Throwable t) {
                qsets.add(t.getMessage());
            }
        });

        playGame.setOnLongClickListener(v -> {

            playGame.setOnTouchListener((v1, event) -> {
                v1.onTouchEvent(event);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //dit is de functie die gecalled wordt als iemand op start game klikt.
                    //TODO: levi je kan hier de namen pakken door arraylist "names" te accessen
                    //TODO: Get ID from the selected_questionset
                    Log.i("Click control: ", "button new game has been clicked");
                    String selected_questionSet = (String) questionSets.getSelectedItem();
                    ApiController.getInstance().createAGame(selected_questionSet, names);

                    Intent intent = new Intent(v1.getContext(), Questions.class);
                    startActivityForResult(intent, 0);
                }
                return false;
            });
            return true;
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}