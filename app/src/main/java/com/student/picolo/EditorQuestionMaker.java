package com.student.picolo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.student.picolo.controller.ApiController;
import com.student.picolo.model.Question;

import java.util.ArrayList;
import java.util.List;

public class EditorQuestionMaker extends AppCompatActivity {

    int next = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<String> questions = new ArrayList<>();
        List<String> titles = new ArrayList<>();
        questions.add("Hoe gaat ie ermee?");
        titles.add("Vraag");
        setContentView(R.layout.cr_qs_editor);
        Button add = (Button) findViewById(R.id.button_add_question);
        EditText title = (EditText) findViewById(R.id.textT);
        EditText question = (EditText) findViewById(R.id.questionT);
        if(!titles.isEmpty()){
            title.setText(titles.get(next));
            question.setText(questions.get(next));
        }
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = titles.size();
                String t = title.toString();
                String q = question.toString();
                if(next < size-1){
                    questions.set(next, q);
                    titles.set(next, t);
                    title.setText(titles.get(next+1));
                    question.setText(questions.get(next+1));
                }
                else if(next == size-1){
                    questions.set(next, q);
                    titles.set(next, t);
                    title.setText("");
                    question.setText("");
                }
                else {
                    title.setText("");
                    question.setText("");
                    questions.add(q);
                    titles.add(t);
                }
                next++;
            }
        });

        Button save = (Button) findViewById(R.id.save_button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < titles.size(); i++) {
                    String que = questions.get(i);
                    int names = (que.length() - que.replaceAll("NAME", "").length()) / 4;
                    Question c = new Question(i, titles.get(i), questions.get(i), names);
                    ApiController.getInstance().addQuestion(c);
                }

                Toast.makeText(EditorQuestionMaker.this, "Saving", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(), HomeScreen.class);
                startActivityForResult(intent, 0);
            }
        });
    }
}
