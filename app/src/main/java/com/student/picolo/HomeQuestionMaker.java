package com.student.picolo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

public class HomeQuestionMaker extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cr_qs_home);
        Button editor = (Button) findViewById(R.id.edit_button);
        Button newSet = (Button) findViewById(R.id.new_button);
        editor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ChooseEditSet.class);
                startActivityForResult(intent, 0);
            }
        });
        newSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), NameQuestionSet.class);
                startActivityForResult(intent, 0);
            }
        });
    }
}
