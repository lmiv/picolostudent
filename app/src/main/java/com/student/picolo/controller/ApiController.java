package com.student.picolo.controller;

import android.util.Log;
import android.widget.TextView;
import com.student.picolo.RetrofitInstance;
import com.student.picolo.RetrofitInterface;
import com.student.picolo.model.Question;
import lombok.Data;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.Arrays;
import java.util.List;

@Data
public class ApiController {

    private static ApiController apiController = null;

    private int currentGameId;

    private int currentQuestionSetId;

    private Question nextQuestion;

    private ApiController() {

    }

    /**
     * This function generates a singleton instance of this class
     *
     * @return ApiController singleton instance
     */
    public static ApiController getInstance() {

        if (apiController == null)
            apiController = new ApiController();

        return apiController;
    }

    /**
     * This function sends a API request to generate a new game and sets the returned gameId as active one.
     *
     * @param questionSetTitle the ID of the question set which is started.
     * @param names         list of string containing the names of the participants
     */
    public void createAGame(String questionSetTitle, List<String> names) {

        Log.i("DEBUG create game", "Create game called with id :" + questionSetTitle + " and names : \n" + names.toString());

        RetrofitInterface retrofitInterface = RetrofitInstance.getRetrofitInstance().create(RetrofitInterface.class);

        //  This part is inplace such that Retrofit sends the proper formatting for a list of strings, such that the API can interpret it correct.
        Iterable names_itr = Arrays.asList(names);
        String str = names_itr.toString().replaceAll("\\s+", "").replaceAll("\\[+", "").replaceAll("\\]+", "");

        //  Creating the API call
        Call<Integer> listCall = retrofitInterface.createGame(questionSetTitle, str);

        //  Enqueue it on another thread
        listCall.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                if (response.isSuccessful()) {
                    //  If we get a successful response we set the currentGameId to the response
                    currentGameId = response.body();
                    Log.i("DEBUG create game call", "response successful with body: " + response.body());
                } else {
                    //  If we get an error response we set the currentGameId to -1
                    currentGameId = -1;
                    Log.i("DEBUG create game call", "response unsuccessful");
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                currentGameId = -1;
                Log.i("DEBUG create call", "no response....");
            }
        });
    }


    /**
     * Sets the next question in a the correct text fields.
     *
     * @param title    the TextView in which the title of the question should be inserted.
     * @param question the TextView in which the content of the question should be inserted
     */
    public void getNextQuestion(TextView question, TextView title) {

        RetrofitInterface retrofitInterface = RetrofitInstance.getRetrofitInstance().create(RetrofitInterface.class);

        Call<Question> listCall = retrofitInterface.getNextQuestion(this.currentGameId);
        listCall.enqueue(new Callback<Question>() {
            @SneakyThrows
            @Override
            public void onResponse(Call<Question> call, Response<Question> response) {

                Log.i("response", response.toString());
                Log.i("nextQuestion response", response.body().toString());

                Question q = response.body();

                question.setText(q.getQuestion());

                //The line beneath is used to change the question title
                title.setText(q.getTitle());
            }

            @Override
            public void onFailure(Call<Question> call, Throwable t) {
                Log.i("DEBUG create questionset Call", "no response...");
            }
        });

    }

    public void addQuestionSet(String title) {
        Log.i("DEBUG create questionset", "Creating questionset with title : " + title);

        RetrofitInterface retrofitInterface = RetrofitInstance.getRetrofitInstance().create(RetrofitInterface.class);

        Call<Integer> listCall = retrofitInterface.addQuestionSet(title);
        listCall.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    //  If we get a successful response we set the currentGameId to the response
                    currentQuestionSetId = response.body();
                    Log.i("DEBUG create questionset call", "response successful with body: " + response.body());
                } else {
                    //  If we get an error response we set the currentGameId to -1
                    currentQuestionSetId = -1;
                    Log.i("DEBUG create questionset call", "response unsuccessful");
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.i("DEBUG create questionset Call", "no response...");
            }
        });

    }

    public void addQuestion(Question question) {

        RetrofitInterface retrofitInterface = RetrofitInstance.getRetrofitInstance().create(RetrofitInterface.class);

        Call<Void> listCall = retrofitInterface.addQuestion(question, currentQuestionSetId);
        listCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.i("DEBUG add question call", "response succesful.");
                } else {
                    Log.i("DEBUG add question call", "response unsuccesful.");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("DEBUG add question call", "no response...");
            }
        });
    }
}
