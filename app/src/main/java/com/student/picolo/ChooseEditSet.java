package com.student.picolo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import com.student.picolo.controller.ApiController;
import com.student.picolo.model.QuestionSet;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class ChooseEditSet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_question_set);
        ListView listView = (ListView) findViewById(R.id.listView);

        List<String> qsets = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, qsets);

        RetrofitInterface retrofitInterface = RetrofitInstance.getRetrofitInstance().create(RetrofitInterface.class);

        Call<List<QuestionSet>> listCall = retrofitInterface.getAllQuestionSets();

        listCall.enqueue(new Callback<List<QuestionSet>>() {
            @Override
            public void onResponse(Call<List<QuestionSet>> call, Response<List<QuestionSet>> response) {

                if (response.isSuccessful()) {

                    String rep = response.toString();
                    Log.i("Response", rep);
                    Log.i("Content", response.body().toString());

                    for (QuestionSet set : response.body()) {
                        qsets.add(set.getTitle());
                        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                        listView.setAdapter(adapter);
                    }
                } else {
                    qsets.add("Code : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<QuestionSet>> call, Throwable t) {
                qsets.add(t.getMessage());
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ApiController.getInstance().setCurrentQuestionSetId(position + 1);
                Intent i = new Intent(view.getContext(), NewQuestionSet.class);
                startActivityForResult(i, 0);
            }
        });

    }
}