package com.student.picolo;

import com.student.picolo.model.Question;
import com.student.picolo.model.QuestionSet;
import retrofit2.Call;
import retrofit2.http.*;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface RetrofitInterface {

    @GET("/api/create-game/{questionSetTitle}")
    Call<Integer> createGame(@Path("questionSetTitle") String title, @Query(value = "names", encoded = true) String names);

    @GET("/api/get-question-sets")
    Call<List<QuestionSet>> getAllQuestionSets();

    @GET("/api/get-question-sets-by-id/{id}")
    Call<List<QuestionSet>> getAllQuestionSetsFromId(@Path("id") String ids);

    @GET("/api/get-next-question/{gameId}")
    Call<Question> getNextQuestion(@Path("gameId") int id);

    @GET("/api/create-question-set/{title}")
    Call<Integer> addQuestionSet(@Path(value = "title", encoded = true) String title);

    @POST("/api/add-question/{questionSetId}")
    Call<Void> addQuestion(@Body Question question, @Path("questionSetId") int questionSetId);

    @POST("/api/update-question-set")
    Call<Void> updateQuestionSet(@Query(value = "questionSet") QuestionSet questionSet);

    @POST("/api/delete-question-set")
    Call<Void> deleteQuestionSet(@Query(value = "questionSet") QuestionSet questionSet);

}
