package com.student.picolo;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.student.picolo.model.QuestionSet;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DownloadQuestionSet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_question_set);

        ListView listView = (ListView) findViewById(R.id.listView);

        List<String> qsets = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, qsets);

        RetrofitInterface retrofitInterface = RetrofitInstance.getRetrofitInstance().create(RetrofitInterface.class);

        Call<List<QuestionSet>> listCall = retrofitInterface.getAllQuestionSets();

        listCall.enqueue(new Callback<List<QuestionSet>>() {
            @Override
            public void onResponse(Call<List<QuestionSet>> call, Response<List<QuestionSet>> response) {

                if (response.isSuccessful()) {

                    String rep = response.toString();
                    Log.i("Response", rep);
                    Log.i("Content", response.body().toString());

                    for (QuestionSet set : response.body()) {
                        qsets.add(set.getTitle());
                        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                        listView.setAdapter(adapter);
                    }
                } else {
                    qsets.add("Code : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<QuestionSet>> call, Throwable t) {
                qsets.add(t.getMessage());
            }
        });
        listView.setOnItemClickListener((parent, view, position, id) -> {
            writeFile(position);
            Toast.makeText(DownloadQuestionSet.this, "Downloading " + listView.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
        });
        listView.setOnItemLongClickListener((parent, view, position, id) -> {
            deleteIdFile(position);
            Toast.makeText(DownloadQuestionSet.this, "Deleting " + listView.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
            return true;
        });
    }

    private void deleteIdFile(int questionsetId) {

        StringBuilder sb = new StringBuilder();

        try {
            FileInputStream fileInputStream = openFileInput("favorite_sets.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                sb.append(lines + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fileOutputStream = openFileOutput("favorite_sets.txt", MODE_PRIVATE)) {
            String content = sb.toString().replaceAll((questionsetId + "\n"), "");
            Log.d("content storage", "Contents of text file: \n" + content);
            fileOutputStream.write(content.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void writeFile(int questionsetId) {
        StringBuilder sb = new StringBuilder();

        try {
            FileInputStream fileInputStream = openFileInput("favorite_sets.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                sb.append(lines + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("content storage", sb.toString());

        String textToSave = String.valueOf(questionsetId);

        try (FileOutputStream fileOutputStream = openFileOutput("favorite_sets.txt", MODE_PRIVATE | MODE_APPEND)) {
            fileOutputStream.write(textToSave.getBytes());
            fileOutputStream.write(System.getProperty("line.separator").getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}