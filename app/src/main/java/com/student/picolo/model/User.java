package com.student.picolo.model;

import lombok.Data;

import java.util.List;

@Data
public class User {
    private int id;
    private String username;
    private String password;
    private List<QuestionSet> createdSets;
    private List<QuestionSet> savedSets;
    
}
