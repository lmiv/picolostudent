package com.student.picolo.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Question {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("question")
    private String question;
    @SerializedName("amountOfNames")
    private int amountOfNames;

    public Question(String title, String question, int amountOfNames) {
        this.title = title;
        this.question = question;
        this.amountOfNames = amountOfNames;
    }
}
