package com.student.picolo.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionSet {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("questions")
    private List<Question> questions;
}
