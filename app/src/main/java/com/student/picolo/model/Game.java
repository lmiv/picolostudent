package com.student.picolo.model;

public interface Game {

   /*
   Gets next question for the specific game
   */
    Question getNextQuestion() throws IllegalAccessException;
}
