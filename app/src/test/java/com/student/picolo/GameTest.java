//package com.student.picolo;
//
//import com.student.picolo.model.Game;
//import com.student.picolo.model.GameImpl;
//import com.student.picolo.model.Question;
//import com.student.picolo.model.QuestionSet;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class GameTest {
//
//    private Game game;
//    private List<String> names;
//    private QuestionSet questionSet;
//
//    @Before
//    public void setUp() throws Exception {
//
//        names = new ArrayList<>();
//
//        names.add("Joost");
//        names.add("Ted");
//        names.add("Lars");
//        names.add("Levi");
//
//        questionSet = new QuestionSet(1, "Test set");
//
//        questionSet.addQuestion(
//                new Question(1, "This is the first question", "NAME and NAME who can drink the most?", 2)
//        );
//        questionSet.addQuestion(
//                new Question(2, "One Night Stand", "NAME if you have had more then 2 one night stands hand out 6 sips, " +
//                        "if not drink them yourself.", 1)
//        );
//        questionSet.addQuestion(
//                new Question(3, "Car brands", "Name different car brands, " +
//                        "the first one to repeat or who doesn't know anything has to trek an adt! NAME begins.", 1)
//        );
//
//        game = new GameImpl(1, questionSet, names);
//    }
//
//    @Test
//    @DisplayName("First question draw")
//    public void testQuestionDraw() {
//        Question nextQuestion = null;
//        try {
//            nextQuestion = game.getNextQuestion();
//        } catch (IllegalAccessException e) {
//            System.out.println("No more questions");
//            e.printStackTrace();
//        }
//
//        System.out.println(nextQuestion);
//
//        nextQuestion = null;
//        try {
//            nextQuestion = game.getNextQuestion();
//        } catch (IllegalAccessException e) {
//            System.out.println("No more questions");
//            e.printStackTrace();
//        }
//
//        System.out.println(nextQuestion);
//
//        nextQuestion = null;
//        try {
//            nextQuestion = game.getNextQuestion();
//        } catch (IllegalAccessException e) {
//            System.out.println("No more questions");
//            e.printStackTrace();
//        }
//
//        System.out.println(nextQuestion);
//    }
//
//}
